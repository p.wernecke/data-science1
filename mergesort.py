#THIS CODE WAS CREATED BY CHATGPT, see Jupyter-Notebook for prompt

def merge_sort(arr):
    """
    Perform merge sort on the provided list.
    
    Args:
    arr (list): The list to be sorted.
    """
    if len(arr) > 1:
        mid = len(arr) // 2
        left_half = arr[:mid]
        right_half = arr[mid:]

        # Recursively sort both halves
        merge_sort(left_half)
        merge_sort(right_half)

        i = j = k = 0

        # Merge the sorted halves
        while i < len(left_half) and j < len(right_half):
            if left_half[i] <= right_half[j]:
                arr[k] = left_half[i]
                i += 1
            else:
                arr[k] = right_half[j]
                j += 1
            k += 1

        # Copy the remaining elements of left_half, if any
        while i < len(left_half):
            arr[k] = left_half[i]
            i += 1
            k += 1

        # Copy the remaining elements of right_half, if any
        while j < len(right_half):
            arr[k] = right_half[j]
            j += 1
            k += 1


# Example usage and plotting
if __name__ == "__main__":
# --- THE FOLLOWING PART WAS OPTIMIZED BY CHATGPT ---
    import matplotlib.pyplot as plt

    def plot_list(data, title, line_style='-', marker='o', color='black'):
        x = range(len(data))
        plt.plot(x, data, linestyle=line_style, marker=marker, color=color, label=title)
        plt.title(title)
        plt.xlabel('Index')
        plt.ylabel('Value')
        plt.grid(True)
        plt.legend()
        plt.show()

    my_list = [54, 26, 93, 17, 77, 31, 44, 55, 20]

    # Plot the list before sorting with high contrast color and distinct marker
    plot_list(my_list, "Before Sorting", line_style='-', marker='o', color='blue')

    # Sort the list
    merge_sort(my_list)

    # Plot the list after sorting with high contrast color and distinct marker
    plot_list(my_list, "After Sorting", line_style='--', marker='x', color='orange')
